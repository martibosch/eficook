from django.shortcuts import get_object_or_404, render

from stores.models import Store

def store_detail(request, store_id):
    store = get_object_or_404(Store, pk=store_id)
    context = {'store': store}
    return render(request, 'stores/store_detail.html', context)


from django.db import models

from products.models import Product

# MODELS

class Company(models.Model):
    name = models.CharField(max_length=50)
    # contact info and so on...

    def __str__(self):
        return self.name

    
class Store(models.Model):
    name = models.CharField(max_length=50)
    location = models.CharField(max_length=200)

    company = models.ForeignKey(Company)

    def __str__(self):
        return self.name


# PROMOTIONS
class Condition(models.Model):
    # ugly but works
    pass
    
class ValueCondition(Condition):
    value = models.PositiveIntegerField()

    def __str__(self):
        return "for a purchase of " + str(value) + " euros"

class ProductCondition(Condition):
    units = models.PositiveIntegerField()
    product = models.ForeignKey(Product)

    def __str__(self):
        return "for the purchase of " + str(units) + " of " + str(product)


class Reward(models.Model):
    product = models.ForeignKey(Product)

class ProductReward(Reward):
    units = models.PositiveIntegerField()

    def __str__(self):
        return str(self.units) + " units of " + str(product) + " for free"

class DiscountReward(Reward):
    discount = models.PositiveIntegerField()

    def __str__(self):
        return str(self.discount) + "% of discount on " + str(product)
    

class BasePromotion(models.Model):
    store = models.ForeignKey(Store)
    points = models.PositiveIntegerField()

    condition = models.ForeignKey(Condition)
    reward = models.ForeignKey(Reward)

    def __str__(self):
        return str(condition) + " get " + str(reward)


## UNUSED CLASSES (for now)

# class DiscountReward(Reward):
#     discount = models.PositiveIntegerField()
    
#     class Meta:
#         abstract = True


# class ProductDiscountReward(DiscountReward):
#     product = models.ForeignKey(Product)

#     def __str__(self):
#         return str(self.discount) + "% of discount on " + str(Product)

# class PurchaseDiscountReward(DiscountReward):
#     def __str__(self):
#         return str(self.discount) + "% of discount on your purchase"


# class PointReward(Reward):
#     value = models.PositiveIntegerField()

from django.conf.urls import include, url
from . import views

urlpatterns = [
    # list views
    # url(r'^$', views.recipe_index, name='index'),
    # detail views
    url(r'^(?P<store_id>\d+)/$', views.store_detail, name='store_detail'),

    
]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0003_ingredient_factor_to_grams'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='recipecategory',
            options={'verbose_name': 'recipe category', 'verbose_name_plural': 'recipe categories'},
        ),
    ]

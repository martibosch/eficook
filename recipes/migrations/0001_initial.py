# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_userprofile_password_set'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ingredient',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(blank=True, null=True, upload_to='ingredients/')),
                ('assertable', models.NullBooleanField()),
                ('serving_size', models.IntegerField(default=100)),
                ('calories', models.IntegerField(default=0)),
                ('calories_from_fat', models.IntegerField(default=0)),
                ('total_fat', models.IntegerField(default=0)),
                ('saturated_fat', models.IntegerField(default=0)),
                ('trans_fat', models.IntegerField(default=0)),
                ('mono_fat', models.IntegerField(default=0)),
                ('polin_fat', models.IntegerField(default=0)),
                ('cholesterol', models.IntegerField(default=0)),
                ('sodium', models.IntegerField(default=0)),
                ('calcium', models.IntegerField(default=0)),
                ('potassium', models.IntegerField(default=0)),
                ('iron', models.IntegerField(default=0)),
                ('total_carbs', models.IntegerField(default=0)),
                ('dietary_fiber', models.IntegerField(default=0)),
                ('sugars', models.IntegerField(default=0)),
                ('added_sugars', models.IntegerField(default=0)),
                ('protein', models.IntegerField(default=0)),
                ('vitamin_a', models.IntegerField(default=0)),
                ('vitamin_b6', models.IntegerField(default=0)),
                ('vitamin_b12', models.IntegerField(default=0)),
                ('vitamin_c', models.IntegerField(default=0)),
                ('vitamin_d', models.IntegerField(default=0)),
                ('vitamin_e', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='IngredientCategory',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name_plural': 'ingreident categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('pub_date', models.DateTimeField(verbose_name='date published', auto_now=True)),
                ('preparation_time', models.PositiveIntegerField()),
                ('cooking_time', models.PositiveIntegerField()),
                ('ready_in_time', models.PositiveIntegerField()),
                ('serves', models.PositiveIntegerField()),
                ('author', models.ForeignKey(to='accounts.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecipeCategory',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name': 'category',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecipeFinalImage',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to='recipes/')),
                ('recipe', models.ForeignKey(to='recipes.Recipe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecipeIngredient',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('quantity', models.PositiveIntegerField()),
                ('ingredient', models.ForeignKey(to='recipes.Ingredient')),
                ('recipe', models.ForeignKey(to='recipes.Recipe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecipeStep',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
                ('image', models.ImageField(blank=True, null=True, upload_to='recipes/')),
                ('recipe', models.ForeignKey(to='recipes.Recipe')),
            ],
            options={
                'ordering': ('id',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('factor_to_grams', models.PositiveIntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='recipeingredient',
            name='unit',
            field=models.ForeignKey(to='recipes.Unit'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='recipe',
            name='categories',
            field=models.ManyToManyField(to='recipes.RecipeCategory'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ingredient',
            name='category',
            field=models.ForeignKey(to='recipes.IngredientCategory'),
            preserve_default=True,
        ),
    ]

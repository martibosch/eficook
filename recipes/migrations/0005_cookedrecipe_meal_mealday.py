# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_auto_20150525_1054'),
        ('recipes', '0004_auto_20150525_0806'),
    ]

    operations = [
        migrations.CreateModel(
            name='CookedRecipe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_cooked', models.DateTimeField(verbose_name='date cooked', auto_now=True)),
                ('serves_cooked', models.IntegerField()),
                ('serves_consumed', models.IntegerField(default=0)),
                ('is_consumed', models.BooleanField(default=False)),
                ('recipe', models.ForeignKey(to='recipes.Recipe')),
                ('user_profile', models.ForeignKey(to='accounts.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Meal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_course_serves', models.IntegerField()),
                ('second_course_serves', models.IntegerField()),
                ('first_course', models.ForeignKey(related_name='firstcourse', to='recipes.CookedRecipe')),
                ('second_course', models.ForeignKey(related_name='secondcourse', to='recipes.CookedRecipe')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MealDay',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day', models.DateField()),
                ('dinner', models.OneToOneField(blank=True, related_name='dinner', null=True, to='recipes.Meal')),
                ('lunch', models.OneToOneField(blank=True, related_name='lunch', null=True, to='recipes.Meal')),
                ('user_profile', models.ForeignKey(to='accounts.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

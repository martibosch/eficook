# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0002_auto_20150325_1914'),
    ]

    operations = [
        migrations.AddField(
            model_name='ingredient',
            name='factor_to_grams',
            field=models.FloatField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1000000)]),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0005_cookedrecipe_meal_mealday'),
    ]

    operations = [
        migrations.AddField(
            model_name='meal',
            name='meal_type',
            field=models.CharField(default=0, max_length=1, choices=[(0, 'Lunch'), (1, 'Dinner')]),
            preserve_default=False,
        ),
    ]

# import calendar
import datetime

from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory

from recipes.models import Ingredient, Recipe, RecipeCategory, RecipeIngredient, RecipeStep, RecipeFinalImage, CookedRecipe, Meal, MealDay
from recipes.forms import RecipeCategoryForm, RecipeForm, RecipeIngredientForm, RecipeStepForm, RecipeFinalImageForm, DeleteRecipeForm, ServesForm, MealForm, MealDayForm
from recipes.utils.perform import PerformRecipeHelperGet, PerformRecipeHelperPost

def recipes(request):
    if request.method == 'POST':
        form = RecipeCategoryForm(request.POST)
        if form.is_valid():
            category = form.cleaned_data['recipe_category']
            recipes = Recipe.objects.recipes_in_category(category)

    else:
        form = RecipeCategoryForm()
        recipes = Recipe.objects.all()
    context = {'form':form, 'recipes':recipes}
    return render(request, 'recipes/recipes.html', context)

def recipe(request, recipe_id):
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    context = {'recipe':recipe}
    return render(request, 'recipes/recipe.html', context)

@login_required
def create_recipe(request):
    RecipeIngredientFormSet = inlineformset_factory(Recipe, RecipeIngredient, form=RecipeIngredientForm, can_delete=True)
    RecipeStepFormSet = inlineformset_factory(Recipe, RecipeStep, form=RecipeStepForm, can_delete=True)
    RecipeFinalImageFormSet = inlineformset_factory(Recipe, RecipeFinalImage, form=RecipeFinalImageForm, can_delete=True)
    if request.method == 'POST':
        form = RecipeForm(request.POST, instance=recipe)
        ingredient_formset = RecipeIngredientFormSet(request.POST, instance=recipe)
        step_formset = RecipeStepFormSet(request.POST, instance=recipe)
        image_formset = RecipeFinalImageFormSet(request.POST, instance=recipe)
        if form.is_valid():
            if ingredient_formset.is_valid():
                if step_formset.is_valid():
                    if image_formset.is_valid():
                        # create recipe and assign it the current user
                        recipe = form.save(commit=False)
                        recipe.author = request.user
                        recipe.save()

                        # assign ingredients to the recipe
                        ingredient_list = ingredient_formset.save(commit=False)
                        for ingredient_form in ingredient_list:
                            ingredient_form.recipe = recipe
                            ingredient_form.save()

                        # assign steps to the recipe
                        step_list = step_formset.save(commit=False)
                        for step_form in step_list:
                            step_form.recipe = recipe
                            step_form.save()

                        # assign images to the recipe
                        image_list = image_formset.save(commit=False)
                        for image_form in image_list:
                            image_form.recipe = recipe
                            image_form.save()              
        
                        messages.add_message(request, messages.SUCCESS, "La receta" + str(recipe) + " ha sido creada con éxito.")

        else:
            # TODO real validation
            messages.add_message(request, messages.SUCCESS, "Error de validación del formulario.")
        
        return HttpResponseRedirect(reverse('accounts:recipes'))

    else:
        form = RecipeForm()
        ingredient_formset = RecipeIngredientFormSet()
        step_formset = RecipeStepFormSet()
        image_formset = RecipeFinalImageFormSet()
        context = {'form': form, 'ingredient_formset': ingredient_formset, 'step_formset': step_formset, 'image_formset': image_formset}
        return render(request, 'recipes/create_recipe.html', context)

@login_required
def update_recipe(request, recipe_id):
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    RecipeIngredientFormSet = inlineformset_factory(Recipe, RecipeIngredient, form=RecipeIngredientForm, can_delete=True)
    RecipeStepFormSet = inlineformset_factory(Recipe, RecipeStep, form=RecipeStepForm, can_delete=True)
    RecipeFinalImageFormSet = inlineformset_factory(Recipe, RecipeFinalImage, form=RecipeFinalImageForm, can_delete=True)
    if request.method == 'POST':
        form = RecipeForm(request.POST, instance=recipe)
        ingredient_formset = RecipeIngredientFormSet(request.POST, instance=recipe)
        step_formset = RecipeStepFormSet(request.POST, instance=recipe)
        image_formset = RecipeFinalImageFormSet(request.POST, instance=recipe)
        if form.is_valid():
            if ingredient_formset.is_valid():
                if step_formset.is_valid():
                    if image_formset.is_valid():
                        form.save()
                        ingredient_formset.save()
                        step_formset.save()
                        image_formset.save()

                        messages.add_message(request, messages.SUCCESS, "La receta " + str(recipe) + " ha sido actualizada con éxito.")

        else:
            # TODO real validation
            messages.add_message(request, messages.ERROR, "Error de validación del formulario.")

        return HttpResponseRedirect(reverse('accounts:recipes'))
                
    else:
        form = RecipeForm(instance=recipe)
        ingredient_formset = RecipeIngredientFormSet(instance=recipe)
        step_formset = RecipeStepFormSet(instance=recipe)
        image_formset = RecipeFinalImageFormSet(instance=recipe)
        context = {'recipe': recipe, 'form': form, 'ingredient_formset': ingredient_formset, 'step_formset': step_formset, 'image_formset': image_formset}
        return render(request, 'recipes/update_recipe.html', context)

@login_required
def delete_recipe(request, recipe_id):
    recipe = get_object_or_404(Recipe, pk=recipe_id)

    if request.method == 'POST':
        form = DeleteRecipeForm(request.POST, instance=recipe)

        if form.is_valid():
            messages.add_message(request, messages.SUCCESS, "La receta " + str(recipe) + " ha sido eliminada con éxito.")

            recipe.delete()
        else:
            # TODO real validation
            messages.add_message(request, messages.ERROR, "Error de validación del formulario.")
            
        return HttpResponseRedirect(reverse('accounts:recipes'))
                
    else:
        form = DeleteRecipeForm(instance=recipe)
        context = {'recipe': recipe, 'form': form}
        return render(request, 'recipes/delete_recipe.html', context)

@login_required
def perform_recipe_serves(request, recipe_id):
    recipe = get_object_or_404(Recipe, pk=recipe_id)    

    if request.method == 'POST':
        form = ServesForm(request.POST)
        if form.is_valid():
            serves = form.cleaned_data['serves']
            # return HttpResponseRedirect('/recipes/perform/' + str(recipe_id) + '/' + str(serves) + '/')
            return HttpResponseRedirect(reverse('recipes:perform', kwargs={'recipe_id': recipe_id, 'serves': serves}))
        else:
            # TODO real validation
            messages.add_message(request, messages.ERROR, "Error de validación del formulario.")

    # this is done if (1) the request is a GET, or (2) the form is not valid
    form = ServesForm(initial={'serves': recipe.serves})
    context = {'recipe': recipe, 'serves_form': form}
    return render(request, 'recipes/perform_recipe.html', context)

@login_required
def perform_recipe(request, recipe_id, serves):
    recipe = get_object_or_404(Recipe, pk=recipe_id)    
    pantry = request.user.userprofile.pantry

    if request.method == 'POST':
        helper = PerformRecipeHelperPost(request.POST, recipe, pantry, int(serves))
        if helper.forms_are_valid():
            # delete the products from the pantry
            helper.delete_products()
           
            # create CookedRecipe object
            cooked_recipe = CookedRecipe(recipe=recipe, user_profile=request.user.userprofile, serves_cooked=serves)
            cooked_recipe.save()
            
            messages.add_message(request, messages.SUCCESS, "Tu despensa se ha actualizado. Ahora ya puedes cocinar la receta.")          
            return HttpResponseRedirect(reverse('recipes:cook', args=(recipe_id,)))
        else:
            # TODO real validation
            formset_delete = helper.get_formset_delete()
            formset_ask = helper.get_formset_ask()

            context = {'recipe': recipe, 'serves': serves, 'formset_delete': formset_delete, 'formset_ask': formset_ask}
            
    else:
        helper = PerformRecipeHelperGet(recipe, pantry, int(serves))
    
        formset_delete = helper.get_formset_delete()
        formset_ask = helper.get_formset_ask()
        list_missing = helper.get_list_missing()
        list_assert = helper.get_list_assert()

        # maybe use a different approach. this is UGLY!
        json_id_matrix = helper.get_json_id_matrix()
        
        context = {'recipe': recipe, 'serves': serves, 'formset_delete': formset_delete, 'formset_ask': formset_ask, 'list_missing': list_missing, 'list_assert': list_assert, 'json_id_matrix': json_id_matrix}
        
    
    return render(request, 'recipes/perform_recipe.html', context)


# meal planner

@login_required
def meal_plan(request):
    user_profile = request.user.userprofile
    today = datetime.date.today()
    tomorrow = today + datetime.timedelta(days=1)

    # get meal days
    today_meals = MealDay.objects.get_or_create_meal_day(user_profile, today)
    tomorrow_meals = MealDay.objects.get_or_create_meal_day(user_profile, tomorrow)

    context = {'today_meals': today_meals, 'tomorrow_meals': tomorrow_meals}

    return render(request, 'recipes/meal_plan.html', context)
    

@login_required
def update_meal_plan(request):
    user_profile = request.user.userprofile
    today = datetime.date.today()
    tomorrow = today + datetime.timedelta(days=1)

    if request.method == 'POST':

        # get meal days
        today_meals = MealDay.objects.get_or_create_meal_day(user_profile, today)
        tomorrow_meals = MealDay.objects.get_or_create_meal_day(user_profile, tomorrow)

        # get meals
        today_lunch_form = MealForm(request.POST, prefix="today_lunch")
        today_dinner_form = MealForm(request.POST, prefix="today_dinner")
        tomorrow_lunch_form = MealForm(request.POST, prefix="tomorrow_lunch")
        tomorrow_dinner_form = MealForm(request.POST, prefix="tomorrow_dinner")

        # save the meals
        today_lunch_form.save()
        today_dinner_form.save()
        tomorrow_lunch_form.save()
        tomorrow_dinner_form.save()

        # assign meals to meal days
        # TODO: DESTROY PREVIOUS MEALS?
        today_meals.lunch = today_lunch_form.save()
        today_meals.lunch = today_dinner_form.save()
        tomorrow_meals.lunch = tomorrow_lunch_form.save()
        tomorrow_meals.lunch = tomorrow_dinner_form.save()
        
        # redirect to meal plan detail view

    else:
        recommended_recipes = Recipe.objects.recommended_recipes(user_profile)
        today_lunch_form = MealForm(queryset=recommended_recipes, meal_type="Lunch", prefix="today_lunch")
        today_dinner_form = MealForm(queryset=recommended_recipes, meal_type="Dinner", prefix="today_dinner")
        tomorrow_lunch_form = MealForm(queryset=recommended_recipes, meal_type="Lunch", prefix="tomorrow_lunch")
        tomorrow_dinner_form = MealForm(queryset=recommended_recipes, meal_type="Dinner", prefix="tomorrow_dinner")

        context = {
            'today': today,
            'tomorrow': tomorrow,
            'today_lunch_form': today_lunch_form,
            'today_dinner_form': today_dinner_form,
            'tomorrow_lunch_form': tomorrow_lunch_form,
            'tomorrow_dinner_form': tomorrow_dinner_form
        }

    return render(request, 'recipes/update_meal_plan.html', context)

def ingredient(request, ingredient_id):
    ingredient = get_object_or_404(Ingredient, pk=ingredient_id)
    ingredient_products = ingredient.product_set.exclude(brand__isnull=True)
    context = {'ingredient':ingredient,'ingredient_products':ingredient_products}
    return render(request, 'recipes/ingredient.html', context)

def category(request, category_id):
    category = get_object_or_404(RecipeCategory, pk=category_id)
    recipes_in_category = Recipe.objects.recipes_in_category(category)
    context = {'category':category,'recipes_in_category':recipes_in_category}
    return render(request, 'recipes/category.html', context)

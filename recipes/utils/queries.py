# from recipes.models import Recipe

# CONSTANT FOR IngredientCategory ids
# TODO: is this the right method?

MEAT = 1
FISH = 2
VEGETABLES = 4
LEGUMES = 5
FRUIT = 6
OIL = 7
CONDIMENTS = 8
SAUCE = 9
SPICES = 10
CEREAL = 11
PASTA = 12
SWEETENER = 13
DAIRY = 14
WATER = 15
EGG = 16
PASTRY = 17

class RecipeQuery(object):
    def __init__(self, recipe, preferences):
        self._serves = recipe.serves
        self._recipe_ingredients = recipe.recipeingredient_set.all()
        self._preferences = preferences
        
    # NUTRITION FACTS
    def calories_per_serve(self):
        total_calories = 0
        for recipe_ingredient in self._recipe_ingredients:
            ingredient_grams_per_serve = recipe_ingredient.get_quantity_in_grams() / self._serves
            ingredient_calories_per_serve = recipe_ingredient.ingredient.calories * ingredient_grams_per_serve / recipe_ingredient.serving_size
            total_calories += ingredient_calories_per_serve

        return total_calories

    # TODO: add proteins, vitamines ... per serve
            

    def _check_for_category(self, category):
        for recipe_ingredient in self._recipe_ingredients:
            category_id = recipe_ingredient.ingredient.category.id
            if category_id == category:
                return True
        return False

    def _check_for_category_list(self, category_list):
        for recipe_ingredient in self._recipe_ingredients:
            category_id = recipe_ingredient.ingredient.category.id
            if category_id in category_list:
                return True
        return False

    # INTOLERANCES AND PARTIAL DIET OPTION CHECKS
    # checks return TRUE if the recipe is ok with preferences
    def no_gluten_check(self):
        # TODO: see how to give alternatives to celiacs
        return not self._check_for_category_list([CEREAL, PASTA, PASTRY, DAIRY])
        
    def no_dairy_check(self):
        return not self._check_for_category(DAIRY)

    def no_fructose_check(self):
        return not self._check_for_category_list([FRUIT, SWEETENER])

    def no_eggs_check(self):
        return not self._check_for_category(EGG)

    def no_alcohol_check(self):
        return False

    def no_meat_check(self):
        return not self._check_for_category(MEAT)
        
    def no_fish_check(self):
        # fish includes seafood
        return not self._check_for_category(FISH)

    def no_vegetables_check(self):
        return not self._check_for_category(VEGETABLES)

    def no_legumes_check(self):
        return not self._check_for_category(LEGUMES)

    # def is_doable_ingredients(self, pantry_ingredients):
    #     # TODO count score of 'how close we are from having all indredients'
    #     for recipe_ingredient in self._recipe_ingredients:
    #         ingredient = recipe_ingredient.ingredient
    #         if ingredient not in pantry_ingredients and not ingredient.assertable:
    #             return False

    #     # if we get here, all the recipe ingredients were either in the pantry or assertable, and thus, the recipe is doable
    #     return True

    def determine_ingredients_availability(self, pantry_ingredients):
        # TODO count score according to weight?
        total_ingredients = 0
        available_ingredients = 0
        for recipe_ingredient in self._recipe_ingredients:
            ingredient = recipe_ingredient.ingredient
            if ingredient.assertable:
                pass
            else:
                total_ingredients += 1
                if ingredient in pantry_ingredients:
                    available_ingredients += 1

        return available_ingredients, total_ingredients

    def is_doable_preferences(self):
        for preference in self._preferences:
            check_method = getattr(self, preference + "_check")
            if not check_method():
                return False

        # if we get here, all the preferences were matched
        return True

    def is_doable(self):
        # see what to do with is_doable_ingredients
        return self.is_doable_preferences()


class RecipesQueries(object):

    def __init__(self, user_profile):

        self._pantry_ingredients = []
        # get all the pantry ingredients
        pantry_products = user_profile.pantry.pantryproduct_set.all().select_related('product').select_related('ingredient')
        for pantry_product in pantry_products:
            self._pantry_ingredients.append(pantry_product.product.ingredient)

        # TODO what about calories?
        # self._user_recipe_preferences = user_profile.userrecipepreferences
        self._recipe_preferences = []
        restriction_fields_iterator = user_profile.userrecipepreferences.iterate_restriction_fields()
        for field_tuple in restriction_fields_iterator:
            if field_tuple[1]:
                self._recipe_preferences.append(field_tuple[0])

    def get_doable_recipes_dict(self, recipe_queryset):
        doable_recipes_dict = {}
        for recipe in recipe_queryset:
            recipe_query = RecipeQuery(recipe, self._recipe_preferences)
            if recipe_query.is_doable():
                available_ingredients, total_ingredients = recipe_query.determine_ingredients_availability(self._pantry_ingredients)
                doable_recipes_dict[available_ingredients / total_ingredients] = (available_ingredients, total_ingredients, recipe)

        doable_recipes = [value for (key, value) in sorted(doable_recipes_dict.items(), reverse=True)]

        return doable_recipes

    def get_doable_recipes_queryset(self, recipe_queryset):
        doable_recipes_ids = []
        for recipe in recipe_queryset:
            recipe_query = RecipeQuery(recipe, self._recipe_preferences)
            if recipe_query.is_doable():
                doable_recipes_ids.append(recipe.pk)

        return recipe_queryset.filter(pk__in = doable_recipes_ids)

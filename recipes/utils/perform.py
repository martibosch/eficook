import json

from django.db import models
from django.forms.formsets import formset_factory

from products.models import Product
from purchases.models import PantryProduct
from recipes.forms import ProductToDeleteForm, ProductToAskForm, BaseProductToDeleteFormSet, BaseProductToAskFormSet
from recipes.models import RecipeIngredient

# COMMON STUFF

DELETE_FLAG = 0
ASK_FLAG = 1

ProductToDeleteFormSet = formset_factory(ProductToDeleteForm, formset=BaseProductToDeleteFormSet, can_delete=False, extra=0)
ProductToAskFormSet = formset_factory(ProductToAskForm, formset=BaseProductToAskFormSet, can_delete=False, extra=0)

def normalize(value):
    return_value = round(value)
    if return_value == 0:
        return 1
    else:
        return return_value

class PerformRecipeHelper(object):
    def __init__(self, serves_factor):
        self._serves_factor = serves_factor

    def _necessary_product_units(self, recipe_ingredient, product):
        """
        Given the recipe_ingredient and the product, return the necessary units of the product needed to cook the recipe
        """
        if product.ingredient_unit == recipe_ingredient.unit:
            return normalize(recipe_ingredient.quantity * self._serves_factor / product.ingredient_quantity)
        else:
            return normalize(recipe_ingredient.get_quantity_in_grams() * self._serves_factor / product.get_quantity_in_grams())


# HANDLE GETS
class PerformRecipeHelperGet(PerformRecipeHelper):

    def __init__(self, recipe, pantry, serves):
        super(PerformRecipeHelperGet, self).__init__(serves/recipe.serves)
        self._recipe_ingredients = recipe.recipeingredient_set.all()
        self._pantry_products = pantry.pantryproduct_set.all()

        # private lists
        self._products_to_delete = []
        self._products_to_ask = []

        # public lists
        self.ingredients_missing = []
        self.ingredients_to_assert = []
        
        # populate the lists
        self._fetch_recipe_products()

        # forms to feed getters
        self.formset_delete = ProductToDeleteFormSet(initial=self._formset_delete_initial_data())

        initial_data, id_matrix = self._formset_ask_initial_data()
        self.formset_ask = ProductToAskFormSet(initial=initial_data, id_matrix=id_matrix)
        # self._formset_ask_fill_choices()
        
        # id matrix choices
        self.json_id_matrix = json.dumps(id_matrix)

    # getters

    def get_list_missing(self):
        return self.ingredients_missing

    def get_list_assert(self):
        return self.ingredients_to_assert

    def get_formset_delete(self):
        if self.formset_delete.total_form_count() == 0:
            return None
        else:
            return self.formset_delete
        
    def get_formset_ask(self):
        if self.formset_ask.total_form_count() == 0:
            return None
        else:
            return self.formset_ask

    def get_json_id_matrix(self):
        if len(self.json_id_matrix) == 0:
            return None
        else:
            return self.json_id_matrix


    # computational functions

    def _fetch_ingredient_products(self, recipe_ingredient):
        """ Given a non-assertable recipe_ingredient, return a list of the following tuple: 
        * id of pantry product
        * product from the pantry that has given ingredient
        * quantity of products that should be consumed     
        * available quantity of products in the pantry
        """
        product_tuple_list = []
        for pantry_product in self._pantry_products:
            product = pantry_product.product
            if recipe_ingredient.ingredient == product.ingredient:
                necessary_units = self._necessary_product_units(recipe_ingredient, product)
                product_tuple_list.append((pantry_product.id, product, necessary_units, pantry_product.quantity)) 
        return product_tuple_list

    def _fetch_recipe_products(self):
        """ Populates the two lists self._products_to_delete and self._products_to_ask
        """
        for recipe_ingredient in self._recipe_ingredients:
            recipe_ingredient.quantity = round(recipe_ingredient.quantity * self._serves_factor)
            if recipe_ingredient.ingredient.assertable:
                self.ingredients_to_assert.append(recipe_ingredient)
            else:
                product_tuple_list = self._fetch_ingredient_products(recipe_ingredient)
                number_of_choices = len(product_tuple_list)
                if number_of_choices == 0:
                    # we don't have that product
                    self.ingredients_missing.append(recipe_ingredient)
                elif number_of_choices == 1:
                    # we only have one product in the pantry
                    self._products_to_delete.append((recipe_ingredient, product_tuple_list[0]))
                else:
                    self._products_to_ask.append((recipe_ingredient, product_tuple_list))

    def _formset_delete_initial_data(self):
        initial_data = []
        for ingredient_product_tuple in self._products_to_delete:
            product_tuple = ingredient_product_tuple[1]
            needed = product_tuple[2]
            available = product_tuple[3]
            # form.fields['quantity'].max_value = available

            if available > needed:
                initial_quantity = needed
            else:
                initial_quantity = available

            form_dict = {
                'recipe_ingredient': ingredient_product_tuple[0],
                'quantity': initial_quantity,
                'pantry_product_id': product_tuple[0],
                'product': product_tuple[1],
                'needed': needed,
                'available': available,
            }

            initial_data.append(form_dict)

        return initial_data

    def _formset_ask_initial_data(self):
        initial_data = []
        id_matrix = []
        for ingredient_products_tuple in self._products_to_ask:
            product_tuples = ingredient_products_tuple[1]
            recipe_ingredient = ingredient_products_tuple[0]
            form_dict = {
                'recipe_ingredient': recipe_ingredient,
                'recipe_ingredient_id': recipe_ingredient.id,
                # 'ingredient_quantity': recipe_ingredient.quantity,
                # 'ingredient_unit_id': recipe_ingredient.unit.id,
            }
            initial_data.append(form_dict)

            id_list = []
            for product_tuple in product_tuples:
                id_list.append(product_tuple[0])
            id_matrix.append(id_list)

        return initial_data, id_matrix
        
    # def _formset_ask_fill_choices(self):
    #     for i, form in enumerate(self.formset_ask):
    #         product_tuples = self._products_to_ask[i][1]
            
    #         id_list = []
    #         for product_tuple in product_tuples:
    #             id_list.append(product_tuple[0])
    #         form.fields['pantry_product'].queryset = PantryProduct.objects.filter(id__in=id_list)

# HANDLE POST
class PerformRecipeHelperPost(PerformRecipeHelper):     
    
    def __init__(self, request_post, recipe, pantry, serves):
        super(PerformRecipeHelperPost, self).__init__(serves/recipe.serves)
        self._pantry_products = pantry.pantryproduct_set.all()
        
        # START UGLY AS HELL
        # ugly formset_delete flag
        formset_delete_flag = request_post.get("formset_delete_flag","")
        # ugly json id matrix
        id_matrix = json.loads(request_post.get("json_id_matrix",""))
        # END UGLY AS HELL

        # forms to feed getters
        if formset_delete_flag != "":
            self.formset_delete = ProductToDeleteFormSet(request_post)
        else:
            self.formset_delete = None
            
        if id_matrix != "":
            self.formset_ask = ProductToAskFormSet(request_post, id_matrix=id_matrix)
        else:
            self.formset_ask = None

    def get_formset_delete(self):
        return self.formset_delete
        
    def get_formset_ask(self):
        return self.formset_ask

    def forms_are_valid(self):
        # delete_valid = self.formset_delete.is_valid()
        # ask_valid = self.formset_ask.is_valid()
        # return delete_valid and ask_valid
        if self.formset_delete == None:
            delete_valid = True
        else:
            delete_valid = self.formset_delete.is_valid()
        if self.formset_ask == None:
            ask_valid = True
        else:
            ask_valid = self.formset_ask.is_valid()

        return delete_valid and ask_valid

    def _delete_product(self, quantity, pantry_product_id):
        pantry_product = self._pantry_products.get(id=pantry_product_id)
        available = pantry_product.quantity

        if quantity < available:
            pantry_product.quantity = available - quantity
            pantry_product.save()
        else:
            pantry_product.delete()

    def _delete_products_iteration(self, flag, form):
        use = form.cleaned_data['use']
        if use:
            if flag == DELETE_FLAG:
                quantity = form.cleaned_data['quantity']
                pantry_product_id = form.cleaned_data['pantry_product_id']
            else:
                # assume flag == ASK_FLAG
                # ingredient_quantity = form.cleaned_data['ingredient_quantity']
                # ingredient_unit_id = form.cleaned_data['ingredient_unit_id']
                # ingredient_unit = Unit.objects.get(id=ingredient_unit_id)
                recipe_ingredient_id = form.cleaned_data['recipe_ingredient_id']
                recipe_ingredient = RecipeIngredient.objects.get(id=recipe_ingredient_id)
                pantry_product = form.cleaned_data['pantry_product']
                pantry_product_id = pantry_product.id

                necessary_units = self._necessary_product_units(recipe_ingredient, pantry_product.product)
                
            self._delete_product(necessary_units, pantry_product_id)

        
    def delete_products(self):
        if self.formset_delete:
            for form in self.formset_delete:
                self._delete_products_iteration(DELETE_FLAG, form)

        if self.formset_ask:
            for form in self.formset_ask:
                self._delete_products_iteration(ASK_FLAG, form)
            

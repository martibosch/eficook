from django import forms
from django.core.exceptions import ValidationError

from datetime import timedelta
import time

class SimpleDurationFormField(forms.IntegerField):
    """ Custom IntegerField that renders a duration in seconds as HH:MM:SS"""
    def prepare_value(self, value):
        if not isinstance(value, (int, long)) and value is not None:
            bits = value.split(':')
            if len(bits) == 3:
                return value
        if isinstance(value, (int, long)) and value >=0:
            return self.convert_seconds_to_bits(value)
        return value

    def clean(self, value):
        bits = value.split(':')
        if value and len(bits) != 3:
            raise ValidationError('Data entered must be in format HH:MM:SS')
        if value and len(bits) == 3:
            return self.convert_bits_to_seconds(bits)

    def convert_bits_to_seconds(self, bits):
        result = timedelta(hours=int(bits[0]), minutes=int(bits[1]), seconds=int(bits[2]))
        return result.seconds

    def convert_seconds_to_bits(self, seconds):
        return time.strftime('%H:%M:%S', time.gmtime(seconds))

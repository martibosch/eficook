from django.conf.urls import include, url
from recipes import views

urlpatterns = [
    #list views
    url(r'^$', views.recipes, name='recipes'),
    url(r'^categories/(?P<category_id>\d+)/$', views.category, name='category'),

    # detail views
    url(r'^(?P<recipe_id>\d+)/$', views.recipe, name='recipe'),
    url(r'^cook/(?P<recipe_id>\d+)/$', views.recipe, name='cook'),
    url(r'^ingredients/(?P<ingredient_id>\d+)/$', views.ingredient, name='ingredient'),

    # recipe CRUD
    url(r'^create/$', views.create_recipe, name='create'),
    url(r'^update/(?P<recipe_id>\d+)/$', views.update_recipe, name='update'),
    url(r'^delete/(?P<recipe_id>\d+)/$', views.delete_recipe, name='delete'),

    # perform recipe
    url(r'^perform/(?P<recipe_id>\d+)/$', views.perform_recipe_serves, name='perform_serves'),
    url(r'^perform/(?P<recipe_id>\d+)/(?P<serves>\d+)/$', views.perform_recipe, name='perform'),

    # plan
    # url(r'^meal_plan/$', views.meal_plan, name='meal_plan'),
    url(r'^update_meal_plan/$', views.update_meal_plan, name='update_meal_plan'),
]

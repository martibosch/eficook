from django.test import TestCase

from purchases.models import Pantry
from recipes.models import Recipe

from recipes.utils import PerformRecipeHelperGet

class PerformRecipeHelperTests(TestCase):
    def test_init_helper(self):
        recipe = Recipe.objects.all()[0]
        serves = 16
        pantry = Pantry.objects.all()[0]
        helper = PerformRecipeHelperGet(recipe, serves, pantry)
        self.assertEqual(True, True)
    

def perform_init_test(r, s, p):
    recipe = Recipe.objects.all()[r]
    pantry = Pantry.objects.all()[p]
    helper = PerformRecipeHelperGet(recipe, s, pantry)
    

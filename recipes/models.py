import datetime

from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.urlresolvers import reverse
from django.db import models#, transaction
from django.utils.html import format_html

from accounts.models import UserProfile
from recipes.utils.queries import RecipesQueries
# from products.models import Product

# MANAGERS

class LeftOversManager(models.Manager):
    def oldest_leftovers(self, user_profile):
        return self.filter(user_profile=user_profile, is_consumed=False).order_by('pub_date')

class RecipeManager(models.Manager):
    def latest_recipes(self):
        return self.order_by('-pub_date')[:5]
        
    def recipes_in_category(self, category):
        return category.recipe_set.all()

    def recommended_recipes(self, user_profile):
        queries_util = RecipesQueries(user_profile)
        return queries_util.get_doable_recipes_queryset(self.all())

    def recommended_recipes_dict(self, user_profile):
        queries_util = RecipesQueries(user_profile)
        return queries_util.get_doable_recipes_dict(self.all())

class MealDayManager(models.Manager):
    def get_or_create_meal_day(self, user_profile, day):
        # deal with 'get' exceptions
        try:
            meal_day = self.get(user_profile=user_profile, day=day)
            return meal_day
        except self.model.DoesNotExist:
            meal_day = MealDay(user_profile=user_profile, day=day)
            meal_day.save()
            return meal_day

# MODELS

class IngredientCategory(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = "ingreident categories"

    # methods
    def __str__(self):
        return self.name

class Ingredient(models.Model):
    name = models.CharField(max_length=50)
    category = models.ForeignKey(IngredientCategory)
    image = models.ImageField(upload_to="ingredients/", blank=True, null=True)

    assertable = models.NullBooleanField(blank=True)

    # gluten = models.NullBooleanField(blank=True)
    
    # if ingredient is normally measured in special units, e.g. eggs, get the factor to grams from here
    factor_to_grams = models.FloatField(blank=True, null=True, validators = [MinValueValidator(0), MaxValueValidator(1000000)])

    # nutrition facts
    serving_size = models.IntegerField(default=100)

    # calories
    calories = models.IntegerField(default=0)
    calories_from_fat  = models.IntegerField(default=0)
    
    # fats
    total_fat = models.IntegerField(default=0)
    saturated_fat = models.IntegerField(default=0)
    trans_fat = models.IntegerField(default=0)
    mono_fat = models.IntegerField(default=0)
    polin_fat = models.IntegerField(default=0)
    

    # cholesterol in mg
    cholesterol = models.IntegerField(default=0)

    # mineral salts in mg
    sodium = models.IntegerField(default=0)
    calcium = models.IntegerField(default=0)
    potassium = models.IntegerField(default=0)
    iron = models.IntegerField(default=0)

    # carbs
    total_carbs = models.IntegerField(default=0)
    dietary_fiber = models.IntegerField(default=0)
    sugars = models.IntegerField(default=0)
    added_sugars = models.IntegerField(default=0)

    # protein
    protein = models.IntegerField(default=0)

    # vitamines in mg
    vitamin_a = models.IntegerField(default=0)
    vitamin_b6 = models.IntegerField(default=0)
    vitamin_b12 = models.IntegerField(default=0)
    vitamin_c = models.IntegerField(default=0)
    vitamin_d = models.IntegerField(default=0)
    vitamin_e = models.IntegerField(default=0)
    
    # methods
    def __str__(self):
        return self.name

class RecipeCategory(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = "recipe category"
        verbose_name_plural = "recipe categories"
    
    # methods
    def __str__(self):
        return self.name    
    
class Recipe(models.Model):
    name = models.CharField(max_length=50)
    author = models.ForeignKey(UserProfile)
    pub_date = models.DateTimeField('date published', auto_now=True)
    preparation_time = models.PositiveIntegerField()
    cooking_time = models.PositiveIntegerField()
    ready_in_time = models.PositiveIntegerField()
    serves = models.PositiveIntegerField()
    categories = models.ManyToManyField(RecipeCategory)
    # dificulty ?
    # ingredients = models.ManyToManyField(RecipeIngredient)
    # instructions = models.TextField()
    
    # manager
    objects = RecipeManager()

    # methods
    def __str__(self):
        return self.name

    def clean(self):
        if self.ready_in_time < self.cooking_time + self.preparation_time:
            raise ValidationError({'preparation_time':["Must be greater than the sum of cooking and preparation time!"]})

class RecipeFinalImage(models.Model):
    recipe = models.ForeignKey(Recipe)
    image = models.ImageField(upload_to="recipes/", blank=True, null=True)

class Unit(models.Model):
    name = models.CharField(max_length=50)
    # at this time we assume that all cooking liquids have a density ~= to water
    # factor such that quantity_in_other_unit * other_unit.factor_to_grams = quantity_in_grams
    # exception if factor_to_grams == 0
    # factor_to_grams = models.PositiveIntegerField()
    factor_to_grams = models.FloatField(validators = [MinValueValidator(0), MaxValueValidator(1000000)])

    # methods
    def __str__(self):
        return self.name
        
    # def convert(self, quantity, unit_to_convert):
    #     return quantity * self.factor_to_grams

class RecipeIngredient(models.Model):
    quantity = models.PositiveIntegerField()
    unit = models.ForeignKey(Unit)
    ingredient = models.ForeignKey(Ingredient)
    recipe = models.ForeignKey(Recipe)

    # methods
    def get_factor_to_grams(self):
        if self.unit.factor_to_grams != 0:
            return self.unit.factor_to_grams
        else:
            if self.ingredient.factor_to_grams:
                return self.ingredient.factor_to_grams
            else:
                # TODO: escape this case in form validation
                return 0

    def get_quantity_in_grams(self):
        return self.quantity * self.get_factor_to_grams()

    def __str__(self):
        return str(self.quantity) + " " + str(self.unit) + " de " + str(self.ingredient.name)

    def html_str(self):
        return format_html(str(self.quantity) + " " + str(self.unit) + " de <a href=\"" + reverse('recipes:ingredient', args=(self.ingredient.id,)) + "\">" + str(self.ingredient) + "</a>")


class RecipeStep(models.Model):
    text = models.TextField()
    image = models.ImageField(upload_to="recipes/", blank=True, null=True)
    recipe = models.ForeignKey(Recipe)
    
    # TODO: change for better ordering system!
    class Meta:
        ordering=('id',)

class CookedRecipe(models.Model):
    recipe = models.ForeignKey(Recipe)
    user_profile = models.ForeignKey(UserProfile)
    date_cooked = models.DateTimeField('date cooked', auto_now=True)
    serves_cooked = models.IntegerField()
    serves_consumed = models.IntegerField(default=0)
    is_consumed = models.BooleanField(default=False)
    
    # manager
    objects = LeftOversManager()

    # methods
    def consume(self, serves):
        total_consumed = self.serves_consumed + serves
        if total_consumed < self.serves_cooked:
            self.serves_consumed = total_consumed
        else:
            # ASSUME total_consumed == self.serves_cooked
            # TODO: see how to deal with total_consumed > self.serves_cooked
            self.serves_consumed = self.serves_cooked
            self.is_finished = True

class Meal(models.Model):
    # user_profile = models.ForeignKey(UserProfile)
    # day = models.DateField()

    # CONTSANTS
    LUNCH = 0
    DINNER = 1
    MEAL_TYPE_CHOICES = ((LUNCH, 'Lunch'), (DINNER, 'Dinner'))

    meal_type = models.CharField(max_length=1, choices=MEAL_TYPE_CHOICES)
    
    first_course = models.ForeignKey(CookedRecipe, related_name="firstcourse")
    first_course_serves = models.IntegerField()

    second_course = models.ForeignKey(CookedRecipe, related_name="secondcourse")
    second_course_serves = models.IntegerField()

class MealDay(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    day = models.DateField()
    
    lunch = models.OneToOneField(Meal, related_name="lunch", blank=True, null=True)
    dinner = models.OneToOneField(Meal, related_name="dinner", blank=True, null=True)

    # manager
    objects = MealDayManager()

from django.contrib import admin
from recipes.models import Unit, Ingredient, IngredientCategory, RecipeCategory, RecipeIngredient, RecipeFinalImage, Recipe, RecipeStep

# to create unit
@admin.register(Unit)
class UnitAdmin(admin.ModelAdmin):
    fields = ['name','factor_to_grams']
    verbose_name = "name"

# to create ingredients
@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields':['name','category','assertable','factor_to_grams']}),
        (None, {'fields':['image',]}),
        ('Nutrition Facts', {
            'classes':('collapse',),
            'fields':[
                'serving_size',
                'calories','calories_from_fat',
                'total_fat', 'saturated_fat','trans_fat','mono_fat','polin_fat',
                'cholesterol',
                'sodium','calcium','potassium','iron',
                'total_carbs', 'dietary_fiber', 'sugars','added_sugars',
                'protein',
                'vitamin_a','vitamin_b6','vitamin_b12','vitamin_c','vitamin_d','vitamin_e'
            ]
        }),
    ]

# to create ingredient categories
@admin.register(IngredientCategory)
class IngredientCategoryAdmin(admin.ModelAdmin):
    fields = ['name']
    verbose_name = "category"
 
# to create recipes
class RecipeCategoriesInline(admin.StackedInline):
    model = Recipe.categories.through
    min_num = 1
    extra = 1
    max_num = 4
    verbose_name = "category"

class RecipeCategoriesAdmin(admin.ModelAdmin):
    inlines = [RecipeCategoriesInline,]

class RecipeIngredientInline(admin.StackedInline):
    model = RecipeIngredient
    min_num = 2
    extra = 2
    verbose_name = "ingredient"

class RecipeFinalImageInline(admin.StackedInline):
    model = RecipeFinalImage
    min_num = 1
    extra = 1
    max_num = 3
    verbose_name = "image"

class RecipeStepInline(admin.StackedInline):
    model = RecipeStep
    min_num = 1
    extra = 2
    verbose_name = "step"

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields':['name']}),
        ('Information', {'fields':[('preparation_time','cooking_time','ready_in_time'),'serves']}),
    ]
    inlines = [RecipeFinalImageInline, RecipeCategoriesInline, RecipeIngredientInline, RecipeStepInline]
    exclude = ('categories',)

    def save_model(self, request, obj, form, change):
        obj.author = request.user.userprofile
        obj.save()

# to create recipe final images
@admin.register(RecipeFinalImage)
class RecipeIngredientAdmin(admin.ModelAdmin):
    fields = ['image']
    verbose_name = "image"



# to create recipe categories
@admin.register(RecipeCategory)
class RecipeCategoriesAdmin(admin.ModelAdmin):
    fields = ['name']
    verbose_name = "category"

# to create recipe ingredients
@admin.register(RecipeIngredient)
class RecipeIngredientAdmin(admin.ModelAdmin):
    fields = ['quantity','unit','ingredient']
    verbose_name = "ingredient"

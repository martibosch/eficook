from django import forms
from django.forms.formsets import BaseFormSet

from purchases.models import PantryProduct
from recipes.models import RecipeCategory, Recipe, RecipeIngredient, RecipeStep, RecipeFinalImage, Meal, MealDay

class RecipeCategoryForm(forms.Form):
    recipe_category = forms.ModelChoiceField(queryset=RecipeCategory.objects.all())

class RecipeIngredientForm(forms.ModelForm):
    class Meta:
        model = RecipeIngredient
        fields = ['quantity', 'unit', 'ingredient']

class RecipeStepForm(forms.ModelForm):
    class Meta:
        model = RecipeStep
        fields = ['text', 'image']

class RecipeFinalImageForm(forms.ModelForm):
    class Meta:
        model = RecipeFinalImage
        fields = ['image']

class RecipeForm(forms.ModelForm):
    class Meta:
        model = Recipe
        fields = ['name', 'preparation_time', 'cooking_time', 'ready_in_time', 'serves', 'categories']

class DeleteRecipeForm(forms.ModelForm):
    class Meta:
        model = Recipe
        fields = []

class ServesForm(forms.Form):
    serves = forms.IntegerField(min_value=1)

# TODO: how to deal with quantities
class ProductToForm(forms.Form):
    # non editable fields: only informative
    recipe_ingredient = forms.CharField(required=False)

    # editable fields
    use = forms.BooleanField(initial=True)

class ProductToDeleteForm(ProductToForm):
    # non editable fields
    pantry_product_id = forms.IntegerField()
    product = forms.CharField(required=False)
    needed = forms.IntegerField(required=False)
    available = forms.IntegerField()
    
    # editable fields
    quantity = forms.IntegerField(min_value=0)

class ProductToAskForm(ProductToForm):
    # non editable fields: only informative
    recipe_ingredient_id = forms.IntegerField()
    # ingredient_quantity = forms.IntegerField(min_value=0)
    # ingredient_unit_id = forms.IntegerField(min_value=0)

    # editable fields
    pantry_product = forms.ModelChoiceField(queryset=PantryProduct.objects.none())
    
    def __init__(self, *args, **kwargs):
        pantry_product_ids = kwargs.pop('pantry_product_ids')
        super(ProductToAskForm, self).__init__(*args, **kwargs)
        self.fields["pantry_product"].queryset = PantryProduct.objects.filter(id__in=pantry_product_ids)

class BaseProductToDeleteFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseProductToDeleteFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False
        
class BaseProductToAskFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        self.id_matrix = kwargs.pop('id_matrix')
        super(BaseProductToAskFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False

    def _construct_form(self, i, **kwargs):
        # kwargs['pantry_products_ids'] = self.id_matrix[i]
        return super(BaseProductToAskFormSet, self)._construct_form(i, pantry_product_ids=self.id_matrix[i])
        

    
# class PerformRecipeForm(forms.Form):

class MealForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        meal_type = kwargs.pop('meal_type')
        queryset = kwargs.pop('queryset')
        super(MealForm, self).__init__(*args, **kwargs)
        self.fields['meal_type'].initial = meal_type
        self.fields['first_course'].queryset = queryset
        self.fields['second_course'].queryset = queryset

    class Meta:
        model = Meal
        fields = ['meal_type','first_course','first_course_serves', 'second_course', 'second_course_serves']    

class MealDayForm(forms.ModelForm):
    # create_lunch = forms.BooleanField()
    # create_dinner = forms.BooleanField()
    class Meta:
        model = MealDay
        # fields = ['day','lunch', 'dinner']
        fields = ['day']

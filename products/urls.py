from django.conf.urls import include, url
from products import views

urlpatterns = [
    # url(r'^$', views.products_index, name='index'),
    url(r'^brands/(?P<brand_id>\d+)/$', views.brand, name='brand'),
    url(r'^(?P<product_id>\d+)/$', views.product, name='product'),
    # top rated, categories...
    
]

from django.core.urlresolvers import reverse
from django.db import models
from django.utils.html import format_html

# MANAGERS

# class ProductManager(models.Manager):        
#     def brand_products(self, brand):
#         return brand.product_set.all()

# MODELS

class Brand(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    image = models.ImageField(upload_to="brands/", blank=True, null=True)

    # methods
    def __str__(self):
        return self.name

    # def html_str(self):
    #     brand_name = str(self)
    #     return format_html("<a href=\"" + reverse('products:brand', args=(self.id,)) + "\">" + brand_name + "</a>")
        

    
class Product(models.Model):
    name = models.CharField(max_length=50)
    brand = models.ForeignKey(Brand, blank=True, null=True)
    image = models.ImageField(upload_to="products/", blank=True, null=True)
    
    # should this be optional? ie frozen prepared food
    ingredient_quantity = models.FloatField(default=1)
    ingredient_unit = models.ForeignKey('recipes.Unit')
    ingredient = models.ForeignKey('recipes.Ingredient')
    
    # ratings ang reviews

    # # manager
    # objects = ProductManager()

    # methods
    def get_factor_to_grams(self):
        if self.ingredient_unit.factor_to_grams != 0:
            return self.ingredient_unit.factor_to_grams
        else:
            if self.ingredient.factor_to_grams:
                return self.ingredient.factor_to_grams
            else:
                # TODO: escape this case in form validation
                return 0

    def get_quantity_in_grams(self):
        return self.ingredient_quantity * self.get_factor_to_grams()

    def __str__(self):
        if self.brand is None:
            return self.name
        else:
            return self.name + " " + str(self.brand)

    def html_str(self):
        product_name = str(self)
        return format_html("<a href=\"" + reverse('products:product', args=(self.id,)) + "\">" + product_name + "</a>")
        

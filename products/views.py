from django.shortcuts import get_object_or_404, render

from products.models import Brand, Product

def brand(request, brand_id):
    brand = get_object_or_404(Brand, pk=brand_id)
    brand_products = brand.product_set.all()
    context = {'brand':brand, 'brand_products':brand_products}
    return render(request, 'products/brand.html', context)

def product(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    context = {'product':product}
    return render(request, 'products/product.html', context)


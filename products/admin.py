from django.contrib import admin
from products.models import Brand, Product

# to create brands
@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    fields = ['name', 'description', 'image']

# to create products
@admin.register(Product)
class BrandAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields':['name', 'brand', 'image']}),
        ('Content', {'fields':[
            'ingredient_quantity',
            'ingredient_unit',
            'ingredient'
        ]})
    ]

from django.shortcuts import render

from recipes.models import Recipe

def index(request):
    # if request.user.is_authenticated():
    #     user_profile = request.user.userprofile
    #     pantry = user_profile.pantry
    #     doable_recipes = Recipe.objects.possible_recipes(user_profile)
    #     context = {'pantry': pantry, 'doable_recipes': doable_recipes}

    # else:
    latest_recipes = Recipe.objects.latest_recipes()
    context = {'latest_recipes':latest_recipes}
        
    return render(request, 'homepage/index.html', context)

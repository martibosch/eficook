from django import forms

from purchases.models import GroceryListProduct, GroceryList, Purchase, PantryProduct

class GroceryListProductForm(forms.ModelForm):
    class Meta:
        model = GroceryListProduct
        fields = ['quantity','product']

class PurchaseForm(forms.ModelForm):
    class Meta:
        model = Purchase
        fields = ['date', 'store']

class PerformPurchaseProductForm(GroceryListProductForm):
    purchased = forms.BooleanField(initial=True, required=True)

class PantryProductForm(forms.ModelForm):
    class Meta:
        model = PantryProduct
        fields = ['quantity','product']

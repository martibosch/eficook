from django.conf.urls import include, url
from purchases import views

urlpatterns = [
    # grocery list
    url(r'^update_grocery_list/$', views.update_grocery_list, name='update_grocery_list'),    
    # pantry
    url(r'^update_pantry/$', views.update_pantry, name='update_pantry'),
    # purchases
    url(r'^create/$', views.create_purchase, name='create_purchase'),
    url(r'^perform/(?P<purchase_id>\d+)/$', views.perform_purchase, name='perform_purchase'),
]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stores', '0002_auto_20150502_2054'),
        ('purchases', '0003_purchase_performed'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchase',
            name='store',
            field=models.ForeignKey(to='stores.Store', default=1),
            preserve_default=False,
        ),
    ]

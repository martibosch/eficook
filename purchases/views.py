from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.forms.models import inlineformset_factory
from django.shortcuts import render

from purchases.models import GroceryList, GroceryListProduct, Purchase, Pantry, PantryProduct
from purchases.forms import GroceryListProductForm, PurchaseForm, PerformPurchaseProductForm, PantryProductForm

# from purchases.models import Purchase, PurchaseProduct, Pantry, PantryProduct
# from purchases.forms import PurchaseProductForm, PurchaseForm, DeletePurchaseForm, PerformPurchaseProductForm, PantryProductForm

@login_required
def update_grocery_list(request):
    grocery_list = request.user.userprofile.grocerylist
    GroceryListProductFormSet = inlineformset_factory(GroceryList, GroceryListProduct, form=GroceryListProductForm, can_delete=True)
    if request.method == 'POST':
        formset = GroceryListProductFormSet(request.POST, instance=grocery_list)
        if formset.is_valid():
            formset.save()

            messages.add_message(request, messages.SUCCESS, "Tu lista de la compra ha sido actualizada con éxito.")
            
        else:
            # TODO real validation
            messages.add_message(request, messages.ERROR, "Error de validación del formulario.")

        return HttpResponseRedirect(reverse('accounts:grocery_list'))
                
    else:
        formset = GroceryListProductFormSet(instance=grocery_list)
        context = {'formset': formset}
        return render(request, 'purchases/update_grocery_list.html', context)
      
@login_required
def create_purchase(request):
    if request.method == 'POST':
        form = PurchaseForm(request.POST)
        if form.is_valid():
            # assign user_profile to purchase
            purchase = form.save(commit=False)
            purchase.user_profile = request.user.userprofile
            purchase.save()

            messages.add_message(request, messages.SUCCESS, "La compra ha sido actualizada con éxito.")
            return HttpResponseRedirect(reverse('accounts:purchases'))
        else:
            messages.add_message(request, messages.ERROR, "Error de validación del formulario.")
            context = {'form': form}
            return render(request, 'purchases/create_purchase.html', context)

    else: # GET
        form = PurchaseForm()
        context = {'form': form}
        return render(request, 'purchases/create_purchase.html', context)        


@login_required
def perform_purchase(request, purchase_id):
    purchase = Purchase.objects.get(id=purchase_id)
    grocery_list = request.user.userprofile.grocerylist
    GroceryListProductFormSet = inlineformset_factory(GroceryList, GroceryListProduct, form=PerformPurchaseProductForm, extra=0)

    if request.method == 'POST':
        formset = GroceryListProductFormSet(request.POST)
        if formset.is_valid():
            # get the request user's pantry products
            user_pantry = request.user.userprofile.pantry
            pantry_products = user_pantry.pantryproduct_set.all()

            # add products to the user's pantry iff they have the "purchased" checkbox marked.
            for form in formset:
                if form.cleaned_data.get('purchased'):
                    form_quantity = form.cleaned_data.get('quantity')
                    form_product = form.cleaned_data.get('product')                    

                    # check if the product is already in the pantry
                    product_in_pantry = False
                    for pantry_product in pantry_products:
                        if form_product == pantry_product.product:
                            # the product is in the pantry: sum of the quantities and update the boolean
                            pantry_product.quantity=pantry_product.quantity+form_quantity
                            pantry_product.save()
                            product_in_pantry = True
                        
                    if not product_in_pantry:
                        # if the product was not in the pantry we add it
                        pantry_product = PantryProduct(pantry=user_pantry, quantity=form_quantity, product=form_product)
                        pantry_product.save()  

                    # delete product from the user's grocery list
                    grocery_list_product = grocery_list.grocerylistproduct_set.get(product=form_product)
                    diff = grocery_list_product.quantity - form_quantity
                    if diff > 0:
                        grocery_list_product.quantity = diff
                        grocery_list_product.save()
                    else:
                        grocery_list_product.delete()
                        
            # set the performed boolean for the purchase
            purchase.performed = True
            purchase.save()
            
            # send the message
            messages.add_message(request, messages.SUCCESS, "La compra ha sido efectuada con éxito, y ahora dispones de sus productos en tu despensa.")
        
        else:
            # TODO real validation
            messages.add_message(request, messages.SUCCESS, "Error de validación del formulario.")            
    
        return HttpResponseRedirect(reverse('accounts:purchases'))

    else:
        formset = GroceryListProductFormSet(instance=grocery_list)
        context = {'formset': formset}
        return render(request, 'purchases/perform_purchase.html', context)


@login_required
def update_pantry(request):
    pantry = request.user.userprofile.pantry

    PantryProductFormSet = inlineformset_factory(Pantry, PantryProduct, form=PantryProductForm, can_delete=True)
    if request.method == 'POST':
        formset = PantryProductFormSet(request.POST, instance=pantry)
        if formset.is_valid():
            formset.save()        
            messages.add_message(request, messages.SUCCESS, "Tu despensa ha sido actualizada con éxito.")

        else:
            # TODO real validation
            messages.add_message(request, messages.SUCCESS, "Error de validación del formulario.")
    
        return HttpResponseRedirect(reverse('accounts:pantry'))

    else:
        formset = PantryProductFormSet(instance=pantry)
        context = {'formset': formset}
        return render(request, 'purchases/update_pantry.html', context)

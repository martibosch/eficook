from django.db import models

from django.template import defaultfilters

from django.utils import timezone

from accounts.models import UserProfile
from products.models import Product
from stores.models import Store

# MANAGERS

class PurchaseManager(models.Manager):
    def user_pending_purchases(self, user_profile):
        return self.filter(user_profile = user_profile, date__gt=timezone.now(), performed=False)

# MODELS

class GroceryList(models.Model):
    user_profile = models.OneToOneField(UserProfile, unique=True, related_name='grocerylist')

    # methods
    def __str__(self):
        return "Lista de la Compra de " + str(self.user_profile)

class Purchase(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    date = models.DateField()
    performed = models.BooleanField(default=False)

    store = models.ForeignKey(Store)

    # manager
    objects = PurchaseManager()

    # methods
    def __str__(self):
        return "Compra del " + defaultfilters.date(self.date, "l j F")


class Pantry(models.Model):
    user_profile = models.OneToOneField(UserProfile, unique=True)

    # methods
    def last_date_added(self):
        products = PantryProduct.objects.pantry_products(self).order_by('-date_added')
        return products[0].date_added

# products
class BaseProduct(models.Model):
    quantity = models.PositiveIntegerField(default=1)
    product = models.ForeignKey(Product)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.product)

    # def html_str(self):
    #     return 
            
    # def __str__(self):
    #     return self.quantity + " de " + str(self.product)


class GroceryListProduct(BaseProduct):
    grocery_list = models.ForeignKey(GroceryList)
    date_added = models.DateField(auto_now=True)

class PantryProduct(BaseProduct):
    pantry = models.ForeignKey(Pantry)
    date_added = models.DateField(auto_now=True)        

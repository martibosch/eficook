from django.conf.urls import include, url
from django.contrib.auth import views as django_views
from . import views

urlpatterns = [
    # external profile view
    url(r'^(?P<user_profile_id>\d+)/$', views.user_profile, name='user_profile'),

    # register, login, logout
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', django_views.login, name='login', kwargs={'template_name':'accounts/login.html'}),
    url(r'^logout/$', django_views.logout, name='logout', kwargs={'next_page':'homepage.views.index'}),

    # user's views
    url(r'^home/$', views.user_home, name='home'),
    url(r'^grocery_list/$', views.user_grocery_list, name='grocery_list'),
    url(r'^pantry/$', views.user_pantry, name='pantry'),
    url(r'^purchases/$', views.user_purchases, name='purchases'),
    url(r'^recipes/$', views.user_recipes, name='recipes'),
    # user's configurations
    url(r'^preferences/$', views.user_preferences, name='preferences'),
    url(r'^configuration/$', views.user_configuration, name='configuration'),
    url(r'^change_password/$', views.user_change_password, name='change_password'),
]

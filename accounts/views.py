from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm, SetPasswordForm

from accounts.forms import UserForm, UserDataForm, UserProfileForm, UserRecipePreferences
from accounts.models import UserProfile
from purchases.models import GroceryList, Purchase, Pantry, PantryProduct
from recipes.models import Recipe

# AUXILIARY METHODS

def generate_user_name(email):
    # get user name from email (just to be ok with django user model)
    return email[:email.find("@")]

def create_pantry(user_profile_instance):
    Pantry.objects.create(user_profile=user_profile_instance)

def create_grocery_list(user_profile_instance):
    GroceryList.objects.create(user_profile=user_profile_instance)

# METHODS

# USER REGISTRATION

def register(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST)
        profile_form = UserProfileForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            # saving the base user
            user = user_form.save()
            # set user name
            user.username = generate_user_name(user_form.cleaned_data['email'])
            # hash the password
            user.set_password(user.password)
            user.save()
            
            # associate user and profile
            profile = profile_form.save(commit=False)
            profile.user = user
            # user signed up through site, he chose his password
            # TODO: what to do with social auth
            profile.password_set = True 
            profile.save()
            
            # create pantry and grocery list
            create_pantry(profile)
            create_grocery_list(profile)

            # message
            messages.add_message(request, messages.SUCCESS, "Has sido registrado con éxito.")
            return HttpResponseRedirect(reverse('accounts:home'))
            
        else:
            # TODO: real validation
            messages.add_message(request, messages.ERROR, "Error en el formulario.")
        
    # NOT a 'POST' request
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()
        
    context = {'user_form': user_form, 'profile_form': profile_form}
    return render(request, "accounts/register.html", context)

# USER VIEWS

@login_required
def user_home(request):
    user_profile = request.user.userprofile
    pantry = user_profile.pantry
    recommended_recipes = Recipe.objects.recommended_recipes_dict(user_profile)
    context = {'pantry': pantry, 'recommended_recipes': recommended_recipes}
    return render(request, 'accounts/user_home.html', context)

@login_required
def user_grocery_list(request):
    grocery_list = request.user.userprofile.grocerylist
    context = {'grocery_list': grocery_list}
    return render(request, 'accounts/user_grocery_list.html', context)

@login_required
def user_pantry(request):
    user_pantry = request.user.userprofile.pantry
    context = {'user_pantry': user_pantry}
    return render(request, 'accounts/user_pantry.html', context)

@login_required
def user_purchases(request):
    pending_purchases = Purchase.objects.user_pending_purchases(request.user.userprofile)
    # pending_purchases = request.user.userprofile.purchase_set.all()
    context = {'pending_purchases': pending_purchases}
    return render(request, 'accounts/user_purchases.html', context)

@login_required
def user_recipes(request):
    user_recipes = request.user.userprofile.recipe_set.all()
    context = {'user_recipes': user_recipes}
    return render(request, 'accounts/user_recipes.html', context)

# USER CONFIGURATION STUFF

@login_required
def user_preferences(request):
    user_recipe_preferences = request.user.userprofile.userrecipepreferences
    if request.method == 'POST':
        preferences_form = UserRecipePreferences(request.POST, instance=user_recipe_preferences)
        if preferences_form.is_valid():
            preferences_form.save()
            messages.add_message(request, messages.SUCCESS, "Tus preferencias han sido actualizadas con éxito.")
            return HttpResponseRedirect(reverse('accounts:home'))
        else:
            # TODO real validation
            messages.add_message(request, messages.ERROR, "Error de validación del formulario.")
    
    else:
        preferences_form = UserRecipePreferences(instance=user_recipe_preferences)
    context = {'preferences_form': preferences_form}
    return render(request, 'accounts/user_preferences.html', context)

@login_required
def user_configuration(request):
    user = request.user
    user_profile = user.userprofile
    if request.method == 'POST':
        data_form = UserDataForm(request.POST, instance=user)
        profile_form = UserProfileForm(request.POST, instance=user_profile)
        if data_form.is_valid() and profile_form.is_valid():
            data_form.save()
            profile_form.save()
            messages.add_message(request, messages.SUCCESS, "Tu perfil ha sido actualizado con éxito.")
            return HttpResponseRedirect(reverse('accounts:home'))
        else:
            # TODO real validation
            messages.add_message(request, messages.ERROR, "Error de validación del formulario.")

    else:
        data_form = UserDataForm(instance=user)
        profile_form = UserProfileForm(instance=user_profile)

    context = {'data_form':data_form, 'profile_form':profile_form}
    return render(request, 'accounts/user_configuration.html', context)

@login_required
def user_change_password(request):
    user_profile = request.user.userprofile
    if request.method == 'POST':
        if user_profile.password_set:
            # user has set his password before at least once
            return user_update_password(request, user_profile)
        else:
            # user has never set his password (social auth)
            return user_create_password(request, user_profile)

    else:
        if user_profile.password_set:
            # user has set his password before at least once
            form = PasswordChangeForm(user=request.user)
        else:
            # user has never set his password (social auth)
            form = SetPasswordForm(user=request.user)

        context = {'user_profile': user_profile, 'form': form}
        return render(request, 'accounts/user_change_password.html', context)

def user_update_password(request, user_profile):
    form = PasswordChangeForm(request.user, request.POST)
    if form.is_valid():
        form.save()
        update_session_auth_hash(request, form.user)
        # message
        messages.add_message(request, messages.SUCCESS, "Tu contraseña ha sido actualizada con éxito.")
        return HttpResponseRedirect(reverse('accounts:home'))

    else:
        context = {'user_profile': user_profile, 'form': form}
        return render(request, 'accounts/user_change_password.html', context)


def user_create_password(request, user_profile):
     form = SetPasswordForm(request.user, request.POST)
     if form.is_valid():
         form.save()
         update_session_auth_hash(request, form.user)

         # mark that the user has already set its password at least once. This field will remain True FOREVER!
         user_profile.password_set = True
         user_profile.save()

         # message
         messages.add_message(request, messages.SUCCESS, "Tu contraseña ha sido guardada con éxito.")
         return HttpResponseRedirect(reverse('accounts:home'))

     else:
         context = {'user_profile': user_profile, 'form': form}
         return render(request, 'accounts/user_change_password.html', context)

def user_profile(request, user_profile_id):
    user_profile = get_object_or_404(UserProfile, pk=user_profile_id)
    user_recipes = user_profile.recipe_set.all()
    context = {'user_profile': user_profile, 'user_recipes': user_recipes}
    return render(request, 'accounts/user_profile.html', context)

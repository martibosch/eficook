from django import forms
from django.forms.widgets import NumberInput
from django.contrib.auth.models import User

from accounts.models import UserProfile, UserRecipePreferences

class UserForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True

    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())
    
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'password')

    def clean(self):
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')
        if password == "":
            raise forms.ValidationError("La contraseña no puede estar vacía")
        if password != confirm_password:
            raise forms.ValidationError("Las contraseñas no coinciden")
        
        return self.cleaned_data

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if email and User.objects.filter(email=email).count() > 0:
            raise forms.ValidationError("Ya existe un usuario con este email.")
        return email

class UserDataForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('gender', 'birthday')

class UserRecipePreferences(forms.ModelForm):
    class Meta:
        model = UserRecipePreferences
        fields = ('no_gluten','no_dairy', 'no_eggs', 'no_meat', 'no_fish', 'no_vegetables')
        # fields = ('calories_per_day','no_gluten','no_dairy','no_eggs', 'no_alcohol', 'no_meat', 'no_fish', 'no_vegetables', 'no_legumes')
        # widgets = {
        #     'calories_per_day': NumberInput(),
        # }

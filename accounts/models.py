from django.db import models

from django.contrib.auth.models import User
from stores.models import Store

class UserProfile(models.Model):
    user = models.OneToOneField(User, unique=True, related_name='userprofile')
    
    GENDER_CHOICES = (('m', 'Male'), ('f', 'Female'))
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    birthday = models.DateField()

    # optional fields
    # description = models.TextField(null=True, blank=True)

    # internal fields
    password_set = models.BooleanField(default=False)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name


class UserRecipePreferences(models.Model):
    user_profile = models.OneToOneField(UserProfile, unique=True)
    
    # calories_per_day = models.IntegerField(default=1800)

    # restriction fields
    no_gluten = models.BooleanField(default=False)
    no_dairy = models.BooleanField(default=False)
    no_eggs = models.BooleanField(default=False)
    no_meat = models.BooleanField(default=False)
    no_fish = models.BooleanField(default=False)
    no_vegetables = models.BooleanField(default=False)

    # no_alcohol = models.BooleanField(default=False)
    # no_legumes = models.BooleanField(default=False)
        
    def iterate_restriction_fields(self):
        restriction_fields = self._meta.fields[1:]
        for field in restriction_fields:
            yield field.name, getattr(self, field.name)


# from django.db import models
# from django.contrib.auth.models import AbstractBaseUser, BaseUserManager as DjangoBaseUserManager

# from model_utils.managers import InheritanceManager

# from stores.models import Store


# class BaseUserManager(DjangoBaseUserManager):
#     def create_user(self, email=None, password=None, **extra_fields):
#         now = timezone.now()
#         email = BaseUserManager.normalize_email(email)
#         user = GenericUser(email=email, is_superuser=False, last_login=now, **extra_fields)
#         user.set_password(password)
#         user.save(using=self._db)
#         return user

#     def create_superuser(self, email, password, **extra_fields):
#         user = self.create_user(email, password, **extra_fields)
#         user.is_superuser = True
#         user.save(using=self._db)
#         return user


# class CallableUser(AbstractBaseUser):
#     """
#     The CallableUser class allows to get any type of user by calling
#     CallableUser.objects.get_subclass(email="my@email.dom") or
#     CallableUser.objects.filter(email__endswith="@email.dom").select_subclasses()
#     """
#     objects = BaseUserManager()

# class AbstractUser(CallableUser):
#     """
#     Here are the fields that are shared among specific User subtypes.
#     Making it abstract makes 1 email possible in each User subtype.
#     """
#     is_superuser = False
#     objects = BaseUserManager()

#     def __str__(self):
#         return self.user.first_name + " " + self.user.last_name

#     class Meta:
#         abstract = True

# class GenericUser(AbstractUser):
#     pass

# class IndividualUser(AbstractUser):
#     GENDER_CHOICES = (('m', 'Male'), ('f', 'Female'))
#     gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
#     birthday = models.DateField()

#     USERNAME_FIELD = 'email'
#     REQUIRED_FIELD = USERNAME_FIELD

#     email = models.EmailField(unique=True)
#     password_set = models.BooleanField(default=False)

# class UserRecipePreferences(models.Model):
#     user_profile = models.OneToOneField(IndividualUser, unique=True)
    
#     # calories_per_day = models.IntegerField(default=1800)

#     # restriction fields
#     no_gluten = models.BooleanField(default=False)
#     no_dairy = models.BooleanField(default=False)
#     no_eggs = models.BooleanField(default=False)
#     no_meat = models.BooleanField(default=False)
#     no_fish = models.BooleanField(default=False)
#     no_vegetables = models.BooleanField(default=False)

#     # no_alcohol = models.BooleanField(default=False)
#     # no_legumes = models.BooleanField(default=False)
        
#     def iterate_restriction_fields(self):
#         restriction_fields = self._meta.fields[1:]
#         for field in restriction_fields:
#             yield field.name, getattr(self, field.name)

# class StoreUser(AbstractUser):
#     store = models.ForeignKey(Store)

#     USERNAME_FIELD = 'username'
#     REQUIRED_FIELD = USERNAME_FIELD

        
# # class StoreProfile(models.Model):
# #     user = models.OneToOneField(User, unique=True, related_name='storeuserprofile')
# #     store = models.ForeignKey(Store, unique=True, related_name='')

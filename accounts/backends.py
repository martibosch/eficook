from django.contrib.auth.models import User

class EmailBackend(object):
    def authenticate(self, username=None, password=None):
        try:
            user=User.objects.get(email=username)
            if user.check_password(password):
                return user
            else:
                return None
        except:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

# from django.db.models import Q

# from accounts.models import GenericUser

# class UsernameOrEmailBackend(object):
#     def authenticate(self, username=None, password=None, **kwargs):
#         try:
#            # Try to fetch the user by searching the username or email field
#             user = MyUser.objects.get(Q(username=username)|Q(email=username))
#             if user.check_password(password):
#                 return user
#         except MyUser.DoesNotExist:
#             # Run the default password hasher once to reduce the timing
#             # difference between an existing and a non-existing user (#20760).
#             MyUser().set_password(password)

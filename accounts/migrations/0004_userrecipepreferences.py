# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_userprofile_password_set'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserRecipePreferences',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('calories_per_day', models.IntegerField(default=1800)),
                ('no_gluten', models.NullBooleanField(default=False)),
                ('no_dairy', models.NullBooleanField(default=False)),
                ('no_eggs', models.NullBooleanField(default=False)),
                ('no_alcohol', models.NullBooleanField(default=False)),
                ('no_meat', models.NullBooleanField(default=False)),
                ('no_fish', models.NullBooleanField(default=False)),
                ('no_vegetables', models.NullBooleanField(default=False)),
                ('no_legumes', models.NullBooleanField(default=False)),
                ('user_profile', models.OneToOneField(to='accounts.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

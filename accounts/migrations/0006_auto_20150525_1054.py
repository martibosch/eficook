# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0005_auto_20150525_1054'),
    ]

    operations = [
        migrations.AddField(
            model_name='userrecipepreferences',
            name='no_alcohol',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userrecipepreferences',
            name='no_dairy',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userrecipepreferences',
            name='no_eggs',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userrecipepreferences',
            name='no_fish',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userrecipepreferences',
            name='no_gluten',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userrecipepreferences',
            name='no_legumes',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userrecipepreferences',
            name='no_meat',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userrecipepreferences',
            name='no_vegetables',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]

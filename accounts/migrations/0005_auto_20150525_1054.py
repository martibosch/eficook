# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_userrecipepreferences'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userrecipepreferences',
            name='no_alcohol',
        ),
        migrations.RemoveField(
            model_name='userrecipepreferences',
            name='no_dairy',
        ),
        migrations.RemoveField(
            model_name='userrecipepreferences',
            name='no_eggs',
        ),
        migrations.RemoveField(
            model_name='userrecipepreferences',
            name='no_fish',
        ),
        migrations.RemoveField(
            model_name='userrecipepreferences',
            name='no_gluten',
        ),
        migrations.RemoveField(
            model_name='userrecipepreferences',
            name='no_legumes',
        ),
        migrations.RemoveField(
            model_name='userrecipepreferences',
            name='no_meat',
        ),
        migrations.RemoveField(
            model_name='userrecipepreferences',
            name='no_vegetables',
        ),
    ]

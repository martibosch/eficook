# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_remove_userprofile_password_set'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='password_set',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
